﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sol_Delegate_WithoutParameter
{
    class Program
    {
        static void Main(string[] args)
        {
            //classic delegate
            CalculationDelegate calDelObj = new CalculationDelegate(new Calculation().Add);
            //calDelObj();

            //lambda expression
            //calDelObj = () =>
            //  {
            //      //new Calculation().Add();

            //      int val3 = 9;
            //      int val4 = 9;
            //      Console.WriteLine(val3 + val4);
            //  };
            //calDelObj();

            //anonymous method

            calDelObj = delegate ()
              {
                  int val5 = 3;
                  int val6 = 5;
                  Console.WriteLine(val5 + val6);
              };
            calDelObj();
        }
    }

    public delegate void CalculationDelegate();

    public class Calculation
    {
        public void Add()
        {
            int val1 = 10;
            int val2 = 20;

            Console.WriteLine(val1 + val2);
        }
    }
}
